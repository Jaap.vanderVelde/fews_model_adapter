# builtin
import logging
import warnings
import re
from csv import reader
from dataclasses import dataclass
from sys import argv
from datetime import datetime, timedelta
from pathlib import Path
from collections import defaultdict
from math import cos, sin, atan, sqrt
# installed
from lxml import etree
from pandas import DataFrame
from netCDF4 import Dataset, date2num, stringtochar
import shapefile
import numpy as np

__VERSION__ = '0.3'


def exit_with_error(msg, code=1):
    logging.error(msg)
    exit(code)


def update_log_folder(dirname):
    # rewrite a new log every run
    logging.basicConfig(filename=Path(dirname) / 'process_evo.log', level=logging.INFO, filemode='w')
    logging.getLogger().addHandler(logging.StreamHandler())


class CriticalError(Exception):
    pass


class XMLDocument:
    def __init__(self, f, ns):
        """
        Convenience class, wrapping a parsed etree XML document for easy access through .xpath()
        :param f: a file-like object with an XML document
        :param ns: a dictionary specifying relevant namespaces as prefix: uri
        """
        self._root = etree.parse(f)
        self._ns = ns

    def xpath(self, query, default=None, single=True, strict=True):
        """
        Performs an xpath query on self._root and returns the result, using the namespaces in self._ns
        :param query:
        :param default: what value to return if the query has no result
        :param single: whether the expected match is a single value
        :param strict: raise an exception if single==True, but more result are found, otherwise simply returns the 1st
        :return: Element.xpath() return value
        """
        result = self._root.xpath(query, namespaces=self._ns)
        if len(result) == 0:
            return default
        else:
            if single:
                if strict and len(result) > 1:
                    raise SyntaxError(f'Expected single result for {query}, but found {len(result)}.')
                return result[0]
            else:
                return result


class EVORunInfo:
    _PI_date_format = '%Y-%m-%d %H:%M:%S'
    _PI_ns = {'PI': 'http://www.wldelft.nl/fews/PI'}

    def __init__(self):
        self.run_info_name = None
        # values from runinfo.xml
        self.log_dir = None
        self.start_time = datetime.now()
        self.t0 = datetime.now()
        self.end_time = datetime.now()
        self.work_dir = '.'
        self.input_netcdf = None
        self.output_netcdfs = []
        self.restart_file_name = None
        self.input_parameters = None
        self.model_parameters = {}
        # evo specific values from runinfo.xml
        self.evo_geo = None
        self.evo_input = None
        self.evo_output = None
        self.xsect_selection = None
        self.model_name = None
        self.adapter_output = None
        # evo specific input file locations
        self.wl_name = None
        self.waves_name = None
        self.evc_name = None
        self.evc_template = None
        self.wkt_template = None
        # holds parsed run file etree
        self._run_file = None
        # holds parsed parameters file etree, if any
        self._parameters_file = None

    def _date_time(self, query, name, optional=False, match=None):
        date = self._run_file.xpath(f'{query}/@date')
        time = self._run_file.xpath(f'{query}/@time')
        if time is None or date is None:
            if optional:
                return None
            else:
                raise SyntaxError(
                    f'No {name} date and time found in run file.')
        else:
            result = datetime.strptime(f'{date} {time}', format(self._PI_date_format))
            if match is not None:
                match_result = self._date_time(match[0], None, optional=True)
                if match_result != result:
                    raise SyntaxError(
                        f'{name} date and time found in run file do not match {match[1]} date and time.')
            return result

    def _existing(self, query, name, is_dir=False, parent=False, optional=False):
        result = self._run_file.xpath(query)
        if result is not None:
            result = str(result)
            if parent:
                if not Path(result).parent.is_dir():
                    raise NotADirectoryError(
                        f'{name} folder {result} not found.')
            elif is_dir:
                if not Path(result).is_dir():
                    raise NotADirectoryError(
                        f'{name} folder {result} not found.')
            elif not Path(result).is_file():
                raise NotADirectoryError(
                    f'{name} file {result} not found.')
        elif not optional:
            raise SyntaxError(
                f'No {name} found in run file.')
        return result

    def read_model_parameters(self):
        with open(self.input_parameters, 'r') as f:
            self._parameters_file = XMLDocument(f, self._PI_ns)

        for group in self._parameters_file.xpath('//PI:parameter', single=False):
            self.model_parameters[group.attrib['id']] = float(
                group.xpath('PI:dblValue/text()', namespaces=self._PI_ns)[0])

    def process_run_file(self, filename):
        """
        A class wrapping the Delft-FEWS runfile.xml, specifically for EVO models
        :param filename: the name of an existing runfile.xml
        """
        self.run_info_name = filename
        with open(filename, 'r') as f:
            self._run_file = XMLDocument(f, self._PI_ns)

        # first obtain the log folder, which may be needed for logging script errors
        self.log_dir = self._run_file.xpath('//PI:properties/PI:string[@key="evo_log"]/@value')
        self.work_dir = self._run_file.xpath('//PI:workDir/text()', default=self.work_dir)
        if self.log_dir is None:
            self.log_dir = Path(self.work_dir) / 'log'
        if not Path(self.log_dir).is_dir():
            raise CriticalError(f'Cannot find log directory {self.log_dir}')
        self.log_dir = str(self.log_dir)

        # finish up work directory, which was read to possibly determine log directory
        if not Path(self.work_dir).is_dir():
            raise NotADirectoryError(f'Cannot find work directory {self.work_dir}')
        self.work_dir = str(self.work_dir)

        self.start_time = self._date_time(
            '//PI:startDateTime', 'start')
        self.end_time = self._date_time(
            '//PI:endDateTime', 'end')
        self.t0 = self._date_time(
            '//PI:time0', 't0')
        if not (self.start_time <= self.t0 <= self.end_time):
            raise CriticalError(f'Start {self.start_time}, T0 {self.t0} and end time {self.end_time} out of order!')

        self.input_netcdf = self._existing(
            '//PI:inputNetcdfFile/text()', 'input netCDF')
        self.xsect_selection = self._existing(
            '//PI:properties/PI:string[@key="xsect_selection"]/@value', 'xsect selection')
        self.input_parameters = self._existing(
            '//PI:inputParameterFile/text()', 'input parameters', optional=True)
        if self.input_parameters is not None:
            self.read_model_parameters()

        # remove 2020-07-17, per request Simone
        # self.output_netcdfs = [str(x) for x in self._run_file.xpath('//PI:outputNetcdfFile/text()', single=False)]

        self.evo_geo = self._existing(
            '//PI:properties/PI:string[@key="evo_geo"]/@value', 'EVO geo', is_dir=True)
        self.evo_input = self._existing(
            '//PI:properties/PI:string[@key="evo_input"]/@value', 'EVO input', is_dir=True)
        self.evo_output = self._existing(
            '//PI:properties/PI:string[@key="evo_output"]/@value', 'EVO output', is_dir=True)

        self.model_name = Path(self.input_netcdf).stem
        if Path(self.input_netcdf).stem != self.model_name:
            raise ValueError(
                f'Model will expected model name {self.model_name} to match input data {self.input_netcdf}')
        self.adapter_output = self._existing(
            '//PI:properties/PI:string[@key="adapter_output"]/@value', 'adapter output', is_dir=True)

        self.restart_file_name = self._existing('//PI:properties/PI:string[@key="restart_file"]/@value', 'Restart file')

        # evo inputs
        self.wl_name = Path(self.evo_input) / (Path(self.input_netcdf).stem + '_water_level.csv')
        self.waves_name = Path(self.evo_input) / (Path(self.input_netcdf).stem + '_waves.csv')
        self.evc_name = Path(self.work_dir) / (Path(self.input_netcdf).stem + '.evc')
        self.evc_template = Path(self.work_dir) / '_template.evc'
        self.wkt_template = Path(self.work_dir) / 'projection.wkt'
        if not Path(self.evc_template).is_file():
            raise SyntaxError(
                f'Working directory {self.work_dir}, contains no _template.evc')


@dataclass
class _XSect:
    chainage: float
    a_line: float
    offset: float
    label: str


class EvoProcessing:
    def __init__(self, run_info_fn):
        self.run_info = EVORunInfo()
        try:
            self.run_info.process_run_file(run_info_fn)
        except CriticalError as e:
            exit_with_error(e)
        except (SyntaxError, FileNotFoundError, NotADirectoryError) as e:
            update_log_folder(self.run_info.log_dir)
            exit_with_error(e)
        update_log_folder(self.run_info.log_dir)

    def pre_process(self):
        # load input data
        ds = Dataset(self.run_info.input_netcdf)

        # export water level csv
        DataFrame(zip(
            [datetime(1970, 1, 1) + timedelta(minutes=int(x)) for x in ds['time']],
            [x[0] for x in ds.variables['WL']]
        )).to_csv(self.run_info.wl_name,
                  index=False, header=['TIME', 'WL'], date_format='%d/%m/%Y %H:%M:%S')

        # export waves csv
        DataFrame(zip(
            [datetime(1970, 1, 1) + timedelta(minutes=int(x)) for x in ds['time']],
            [x[0] for x in ds.variables['WVHT']],
            [x[0] for x in ds.variables['WVPER']],
            [x[0] for x in ds.variables['WVDIR']]
        )).to_csv(self.run_info.waves_name,
                  index=False, header=['TIME', 'WVHT', 'WVPER', 'WVDIR'], date_format='%d/%m/%Y %H:%M:%S')

        # generate .evc
        date_time_format = '%d/%m/%Y %H:%M:%S'
        substitutions = {
            'in': self.run_info.evo_input,
            'out': self.run_info.evo_output,
            'geo': self.run_info.evo_geo,
            'log': self.run_info.log_dir,
            'start': datetime.strftime(self.run_info.start_time, date_time_format),
            'end': datetime.strftime(self.run_info.end_time, date_time_format),
            'wl': self.run_info.wl_name,
            'waves': self.run_info.waves_name,
            'state': self.run_info.restart_file_name
        }
        with open(self.run_info.evc_template, 'r') as evc_template:
            with open(self.run_info.evc_name, 'w') as evc:
                copying = True
                log_replaced = False
                header_written = False
                for line in evc_template:
                    if line[:4] == '! //':
                        copying = not copying
                        if not copying and not header_written:
                            evc.write((
                                f'! This file was generated by `process_evo.py {__VERSION__} on {datetime.now()}\n'
                                f'! created as: {self.run_info.evc_name}\n'
                                f'! based on  : {self.run_info.evc_template}\n'
                                f'! run info  : {self.run_info.run_info_name}\n'))
                        continue
                    if copying:
                        if '{log}' in line:
                            if self.run_info.log_dir is None:
                                raise SyntaxError('Template .evc requires log dir, but none provided in run info.')
                            else:
                                log_replaced = True
                        evc.write(line.format(**substitutions))
                    elif re.match(r'.*Version: .*', line):
                        m = re.findall(r'Version: (\d+\.\d+)', line)
                        if len(m) == 0 or m[0] != __VERSION__:
                            raise SyntaxError(
                                f'Template .evc version mismatch, need {__VERSION__}, '
                                f'got {"none" if not m else m[0]}')

                if not log_replaced and self.run_info.log_dir is not None:
                    evc.write(f'\n\n! added by process_evo to override log dir\nLogdir == {self.run_info.log_dir}\n')

    @staticmethod
    def _interpolated(values):
        keys = sorted(values.keys())
        result = {}
        for key in range(keys[0], keys[-1]+1):
            if key in keys:
                result[key] = values[key]
            else:
                prev_key = next(k for k in reversed(keys) if k < key)
                next_key = next(k for k in keys if k > key)
                result[key] = (
                        values[prev_key] +
                        (key - prev_key) * (values[next_key] - values[prev_key]) / (next_key - prev_key)
                )
        return result

    @staticmethod
    def _save_xsect_netcdf4(df, xsect, fn):
        # total length of reach_segment_id values
        blen = len(xsect)
        slen = len(xsect) + 7
        ds = Dataset(fn, mode='w', format='NETCDF4')

        # set up dimensions
        ds.createDimension('time', len(df.index))
        ds.createDimension('id', len(df.columns))
        ds.createDimension('id_charlen', slen)
        ds.createDimension('eta_charlen', blen)

        # set up variables
        # var time
        times = ds.createVariable('time', 'f8', ('time',))
        times.units = 'days since 2020-01-01 00:00:00'
        times.long_name = 'time'
        # var reach_segment_id
        reach_segment_ids = ds.createVariable('reach_segment_id', f'S1', ('id', 'id_charlen',))
        reach_segment_ids.units = '-'
        reach_segment_ids.long_name = 'reach_segment_id'
        reach_segment_ids.cf_role = 'timeseries_id'
        # var z
        zs = ds.createVariable('z', 'f8', ('id', 'time',))
        zs.long_name = 'elevation'
        zs.units = 'm AHD'
        zs.coordinates = 'reach_segment_id'

        # assign data
        times[:] = [date2num(d, 'days since 2020-01-01 00:00:00') for d in df.index]
        with warnings.catch_warnings():
            # this warning is explicitly ignored, but would because by netcdf4.stringtochar() below
            warnings.simplefilter("ignore", category=DeprecationWarning)
            reach_segment_ids[:] = stringtochar(
                np.array([f'{xsect}.CH{str(x).zfill(4)}' for x in df.columns], f'S{slen}'))
        zs[:, :] = df.transpose()
        ds.close()

    class ALine:
        def __init__(self, fn, fn_offset):
            self._data = []
            with open(fn, 'r') as f:
                with open(fn_offset, 'r') as f_offset:
                    cr = reader(f)
                    cr_offset = reader(f_offset)
                    # skip the headers, assuming chainage, distance; also assumes data is sorted
                    next(cr)
                    next(cr_offset)
                    # ignoring a-line z, combining seawall and offset in result
                    for (chainage, aline, _), (chainage_offset, offset) in zip(cr, cr_offset):
                        if chainage != chainage_offset:
                            raise ValueError('Mismatch in defined chainage between a-line and offsets')
                        self._data.append(_XSect(float(chainage), float(aline), float(offset), ''))

        def for_chainage(self, chainage) -> tuple:
            # determine A-line distance and offset from point before and after chainage, snapping to closest centre
            # line for cell based on approach in Matlab scripts of Pamela Wong, after discussing with Pam 2020-08-27
            # 'snap' outside
            if chainage < self._data[0].chainage:
                return self._data[0].a_line, self._data[0].offset
            elif chainage > self._data[-1].chainage:
                return self._data[-1].a_line, self._data[-1].offset
            # find snap inside
            for a, b in zip(self._data, self._data[1:]):
                if a.chainage <= chainage <= b.chainage:
                    if chainage - a.chainage < b.chainage - chainage:
                        return a.a_line, a.offset
                    else:
                        return b.a_line, b.offset

    class Grid:
        def __init__(self, fn):
            self._grid = None
            with open(fn, 'r') as f:
                cr = reader(f)
                # skip the header, assuming chainage, distance; also assumes data is sorted
                next(cr)
                self._grid = [(float(chainage), (float(x1), float(y1)), (float(x2), float(y2)))
                              for chainage, x1, y1, x2, y2 in cr]

            self._xsects = {
                (ch1 + ch2) / 2: (((x11 + x12) / 2, (y11 + y12) / 2), ((x21 + x22) / 2, (y21 + y22) / 2))
                for (ch1, (x11, y11), (x21, y21)), (ch2, (x12, y12), (x22, y22)) in zip(self._grid, self._grid[1:])
            }

        def coords_for_chainage(self, shore_chainage, xsect_chainage, xsect_length):
            for gl1, gl2 in zip(self._grid, self._grid[1:]):
                if gl2[0] > shore_chainage:
                    r = (shore_chainage - gl1[0]) / (gl2[0] - gl1[0])
                    xsect_x1 = gl1[1][0] + (gl2[1][0] - gl1[1][0]) * r  # ratio between x's of start of gridlines
                    xsect_y1 = gl1[1][1] + (gl2[1][1] - gl1[1][1]) * r  # ratio between y's of start of gridlines
                    xsect_x2 = gl1[2][0] + (gl2[2][0] - gl1[2][0]) * r  # ratio between x's of end of gridlines
                    xsect_y2 = gl1[2][1] + (gl2[2][1] - gl1[2][1]) * r  # ratio between y's of end of gridlines

                    # determine point along line based on the ratio of chainage along xsect to xsect length
                    x = xsect_x1 + (xsect_x2 - xsect_x1) * (xsect_chainage / xsect_length)
                    y = xsect_y1 + (xsect_y2 - xsect_y1) * (xsect_chainage / xsect_length)
                    return x, y
            raise ValueError('Chainage outside grid')

    @staticmethod
    def _clean_label(label):
        # leave empty labels empty, match others to specific format
        return '' if not label else re.match(r'(\w+\d*_\d+)', label.strip()).group(1)

    def _read_needed_xsects(self, a_line):
        # read locations for which outputs to FEWS should be generated, as `xsects`, given an ALine
        with open(self.run_info.xsect_selection, 'r') as f:
            cr = reader(f)
            header = next(cr)
            id_col = header.index('ID')
            chainage_col = header.index('CHAINAGE')
            label_col = header.index('Label')
            result = {}
            for row in cr:
                x, offset = a_line.for_chainage(float(row[chainage_col]))
                result[row[id_col]] = _XSect(
                    float(row[chainage_col]), x, offset, self._clean_label(row[label_col])
                )
            return result

    def _read_evo_xsect_output(self, xsects):
        # read EVO output for xsects in `xsects` (or all xsects if `xsects` is None)
        xsect_data = defaultdict(dict)
        xsect_filename = Path(self.run_info.evo_output) / (self.run_info.model_name + '_xsect.csv')
        t0 = self.run_info.t0
        with open(xsect_filename) as f:
            try:
                header = next(f).split(',')
                assert header[0] == 'XSECT_ID'
                try:
                    while True:
                        x_rec = next(f).split(',')
                        assert x_rec[0][:3] == 'XS_'
                        n = x_rec[0][3:]
                        if n in xsects:
                            assert x_rec[1] == 'X'
                            z_rec = next(f).split(',')
                            assert z_rec[0] == x_rec[0]
                            assert z_rec[1] == 'Z'
                            dt = datetime.strptime(x_rec[2], '%d/%m/%Y %H:%M:%S')
                            if dt >= t0:
                                xsect_data[n][dt] = {
                                    # shift x by a_line - offset
                                    # subtract a_line, to line up profile with reach_segment_ids calibrated on survey
                                    # then *add* offset, shifting back seaward for a positive offset
                                    round(float(x) - xsects[n].a_line + xsects[n].offset):
                                        float(z) for x, z in zip(x_rec[3:], z_rec[3:])
                                }
                        else:
                            next(f)
                        next(f)
                        next(f)
                except StopIteration:
                    pass
            except (AssertionError, ValueError):
                exit_with_error(
                    f'Error processing {xsect_filename}')
        return xsect_data

    def _process_xsects(self):
        a_line = self.ALine(f'{self.run_info.evo_geo}/EVO_PostProcess_A-Line.csv')

        xsects = self._read_needed_xsects(a_line)
        xsect_data = self._read_evo_xsect_output(xsects)

        # for each of the selected time series, write a netCDF
        for xsect, time_series in xsect_data.items():
            df = DataFrame([
                {**{'time': ts}, **self._interpolated(data)} for ts, data in time_series.items()
            ]).set_index('time')
            xsect_label = xsects[xsect].label
            if xsect_label:
                self._save_xsect_netcdf4(df, xsect, f'{Path(self.run_info.adapter_output)}/{xsect_label}.nc')

    @staticmethod
    def _extend_shoreline(coords, r=.4):
        # arbitrary offset, into the water, assuming the data follows the coast counter-clockwise
        # assuming at least 2 xsects, otherwise division by 0!
        x1 = coords[-1][0] - coords[0][0]
        y1 = coords[-1][1] - coords[0][1]
        theta = atan(y1 / x1)
        x2 = -sin(theta) * r * sqrt(x1 ** 2 + y1 ** 2)
        y2 = cos(theta) * r * sqrt(x1 ** 2 + y1 ** 2)

        return [
            coords +
            [[coords[-1][0] + x2, coords[-1][1] + y2]] +
            [[coords[-1][0] + x2 - x1, coords[-1][1] + y2 - y1]]
        ]

    def _process_shoreline(self):
        shoreline_data = {}
        with open(f'{self.run_info.evo_output}/{self.run_info.model_name}_shoreline.csv', 'r') as f:
            # skipping header
            next(f)
            try:
                while True:
                    rec_x = next(f).split(',')
                    x = [float(s) for s in rec_x[2:]]
                    assert rec_x[0] == 'X'
                    rec_y = next(f).split(',')
                    y = [float(s) for s in rec_y[2:]]
                    assert rec_y[0] == 'Y'
                    assert rec_y[1] == rec_x[1]

                    # arbitrary offset, into the water, assuming the data follows the coast counter-clockwise
                    r = .4
                    x1 = x[-1] - x[0]
                    y1 = y[-1] - y[0]
                    theta = atan(y1 / x1)
                    x2 = -sin(theta) * r * sqrt(x1 ** 2 + y1 ** 2)
                    y2 = cos(theta) * r * sqrt(x1 ** 2 + y1 ** 2)

                    shoreline_data[datetime.strptime(rec_x[1], '%d/%m/%Y %H:%M:%S')] = [
                        [[a, b] for a, b in zip(x, y)] +
                        [[x[-1] + x2, y[-1] + y2]] + [[x[-1] + x2 - x1, y[-1] + y2 - y1]]
                    ]
            except StopIteration:
                pass

        self._write_shoreline(shoreline_data.items())

    def post_process(self):
        self._process_xsects()
        self._process_shoreline()

    def _process_full_xsects_generate_shoreline(self):
        a_line = self.ALine(
            f'{self.run_info.evo_geo}/GC_EVO_SEAWALL.csv',
            f'{self.run_info.evo_geo}/EVO_PostProcess_Profile_Offset.csv'
        )

        xsects = self._read_needed_xsects(a_line)
        xsect_data = self._read_evo_xsect_output(xsects)

        # determine cumulative daily minimum elevation (maximum erosion) for each xsect, assign that value to 00:00:00
        arbitrary_loc = next(iter(xsect_data.items()))[1]
        # compute the start and end date to iterate over
        start_date, end_date = min(arbitrary_loc.keys()).date(), max(arbitrary_loc.keys()).date()

        # days with data
        days = list(datetime.combine(start_date + timedelta(days=n), datetime.min.time())
                    for n in range((end_date - start_date).days))

        # set up shoreline, finding at HAT=1.15 as a default, can be passed as a model parameter from FEWS
        hat = 1.15 if 'HAT' not in self.run_info.model_parameters else self.run_info.model_parameters['HAT']
        shoreline = {d: [] for d in days}
        grid = self.Grid(f'{self.run_info.evo_geo}/GC_EVO_GRID.csv')

        for xsect, time_series in xsect_data.items():
            # set up chainage for xsect as a list, ranging from minimum chaining to maximum chainage in increments of 1
            chainage = sorted(next(iter(time_series.items()))[1].keys())
            chainage = list(range(chainage[0], chainage[-1]+1))
            # interpolate to the 1m intervals
            xsect_interpolated_elevation_time_series = {
                ts: self._interpolated(data) for ts, data in time_series.items()
            }
            # cumulate minimum elevation to whole days, assign to midnight at the start of the day
            # for each day, the lowest elevation seen on that day or before (within `days`)
            xsect_interpolated_elevation_time_series = {
                d: {
                    c: min(rec[c] for rd, rec in xsect_interpolated_elevation_time_series.items()
                           if rd.date() <= d.date())
                    for c in chainage
                }
                for d in days
            }
            # create dataframe and write netCDF, if the xsect was labeled in xsect_ids
            df = DataFrame([
                {**{'time': ts}, **data} for ts, data in xsect_interpolated_elevation_time_series.items()
            ]).set_index('time')

            xsect_label = xsects[xsect].label
            if xsect_label:
                self._save_xsect_netcdf4(df, xsect_label, f'{Path(self.run_info.adapter_output)}/{xsect_label}.nc')

            # generate shoreline from all xsect data, regardless of label
            for d in days:
                under_hat = False
                for c in reversed(chainage):
                    if xsect_interpolated_elevation_time_series[d][c] < hat:
                        under_hat = True
                    elif under_hat:
                        coords = list(
                            grid.coords_for_chainage(
                                xsects[xsect].chainage,
                                # EVO chainage from 0, ignoring the a-line, but including the offset
                                c - chainage[0] + a_line.for_chainage(xsects[xsect].chainage)[1],
                                len(chainage)-1
                            )
                        )
                        shoreline[d].append(coords)
                        break

        for d, coords in shoreline.items():
            shoreline[d] = self._extend_shoreline(coords)

        self._write_shoreline(shoreline.items())

    def _initial_shoreline(self):
        a_line = self.ALine(
            f'{self.run_info.evo_geo}/GC_EVO_SEAWALL.csv',
            f'{self.run_info.evo_geo}/EVO_PostProcess_Profile_Offset.csv'
        )

        xsects = self._read_needed_xsects(a_line)
        xsect_data = self._read_evo_xsect_output(xsects)

        # determine cumulative daily minimum elevation (maximum erosion) for each xsect, assign that value to 00:00:00
        arbitrary_loc = next(iter(xsect_data.items()))[1]
        # compute the start and end date to iterate over
        start_date, end_date = min(arbitrary_loc.keys()).date(), max(arbitrary_loc.keys()).date()

        # set up shoreline, finding at HAT=1.15 as a default, can be passed as a model parameter from FEWS
        hat = 1.15 if 'HAT' not in self.run_info.model_parameters else self.run_info.model_parameters['HAT']
        shoreline = []
        grid = self.Grid(f'{self.run_info.evo_geo}/GC_EVO_GRID.csv')

        for xsect, time_series in xsect_data.items():
            # set up chainage for xsect as a list, ranging from minimum chaining to maximum chainage in increments of 1
            chainage = sorted(next(iter(time_series.items()))[1].keys())
            chainage = list(range(chainage[0], chainage[-1]+1))
            # interpolate to the 1m intervals
            xsect_interpolated_elevation_time_series = {
                ts: self._interpolated(data) for ts, data in time_series.items()
            }
            # cumulate minimum elevation to whole days, assign to midnight at the start of the day
            # for each day, the lowest elevation seen on that day or before (within `days`)
            _, xsect_interpolated_elevation = next(iter(xsect_interpolated_elevation_time_series.items()))

            # generate shoreline from all xsect data, regardless of label
            under_hat = False
            for c in reversed(chainage):
                if xsect_interpolated_elevation[c] < hat:
                    under_hat = True
                elif under_hat:
                    coords = list(
                        grid.coords_for_chainage(
                            xsects[xsect].chainage,
                            # EVO chainage from 0, ignoring the a-line, but including the offset
                            c - chainage[0] + a_line.for_chainage(xsects[xsect].chainage)[1],
                            len(chainage)-1
                        )
                    )
                    shoreline.append(coords)
                    break

        self._write_single_shoreline(shoreline)

    def _write_shoreline(self, shoreline):
        with open(self.run_info.wkt_template, 'r') as f:
            wkt = f.read()

        for d, coords in shoreline:
            d = datetime.strftime(d, 'shoreline_%Y%m%d_%H%M%S')
            with shapefile.Writer(f'{Path(self.run_info.adapter_output)}/{d}') as shp:
                shp.field('name', 'C')
                shp.poly(coords)
                shp.record('shoreline')
            with open(f'{Path(self.run_info.adapter_output)}/{d}.prj', 'w') as fwkt:
                fwkt.write(wkt)

    def _write_single_shoreline(self, shoreline):
        with open(self.run_info.wkt_template, 'r') as f:
            wkt = f.read()

        with shapefile.Writer(f'output/initial_shoreline.shp') as shp:
            shp.field('name', 'C')
            shp.line([shoreline])
            shp.record('shoreline')
        with open(f'{Path(self.run_info.adapter_output)}/initial_shoreline.prj', 'w') as fwkt:
            fwkt.write(wkt)

    def post_process2(self):
        self._process_full_xsects_generate_shoreline()

    def initial_shoreline(self):
        self._initial_shoreline()


def main():
    if len(argv) < 3 or argv[1].lower() not in ['pre', 'post', 'post2', 'shoreline']:
        exit_with_error('process_evo.py requires two arguments: pre/post[2] and the location of the FEWS runinfo.xml')
    if not Path(argv[2]).is_file():
        exit_with_error(f'No runinfo.xml found at: {argv[2]}')

    try:
        ep = EvoProcessing(argv[2])

        if argv[1].lower() == 'pre':
            ep.pre_process()
        elif argv[1].lower() == 'post':
            ep.post_process()
        elif argv[1].lower() == 'shoreline':
            ep.initial_shoreline()
        else:
            ep.post_process2()
    except Exception as e:
        exit_with_error(f'Unexpected exception raised: {e}')

    logging.info('EVO processing complete')


if __name__ == '__main__':
    main()
