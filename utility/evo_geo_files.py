from math import sqrt
import pandas as pd


def wkt_line(df, point_columns):
    """
    For a DataFrame containing points on lines, return a well-known text series with that line
    :param df: dataframe with at least every column in the series of x, y pairs in point_columns
    :param point_columns: series of x, y pairs from which to construct a linestring well-known text element
    :return: Series
    """
    locs = [(df.columns.get_loc(x), df.columns.get_loc(y)) for x, y in point_columns]
    return df.apply(
        lambda row: f'LINESTRING ({",".join(str(row[x])+" "+str(row[y]) for x, y in locs)})', axis=1)


def wkt_point(df, point_columns):
    """
    For a DataFrame containing points, return a well-known text series with that point
    :param df: dataframe with at least every column in the x, y pair in point_columns
    :param point_columns: x, y pair from which to construct a point well-known text element
    :return: Series
    """
    locs = (df.columns.get_loc(point_columns[0]), df.columns.get_loc(point_columns[1]))
    return df.apply(
        lambda row: f'POINT ({str(row[locs[0]])+" "+str(row[locs[1]])})', axis=1)


def xsects(grid_df, chainage_col, base_cols, end_cols, xsect_chainage):
    """
    From a grid DataFrame, containing grid lines from base_cols to end_cols and chainage in chainage_col, compute
    cross-sections at chainage xsect_chainage
    :param grid_df: dataframe with at least chainage_col column and columns in base_cols, end_cols
    :param chainage_col: column in grid_df and result with chainage for grid lines and xsect, resp.
    :param base_cols: tuple of columns in grid_df and result with base x, y for grid lines and xsect, resp.
    :param end_cols: tuple of columns in grid_df and result with end x, y for grid lines and xsect, resp.
    :param xsect_chainage: series of chainage values for which to compute xsects
    :return: DataFrame with xsects for each xsect_chainage
    """
    grid_df.sort_values(by=[chainage_col])
    xsect_chainage.sort_values()

    lchainage_col = grid_df.columns.get_loc(chainage_col)
    lbase_cols = (grid_df.columns.get_loc(base_cols[0]), grid_df.columns.get_loc(base_cols[1]))
    lend_cols = (grid_df.columns.get_loc(end_cols[0]), grid_df.columns.get_loc(end_cols[1]))

    rows = grid_df.itertuples(index=False)
    r = 0
    p = None
    n = next(rows)
    xsects_result = []
    for chainage in xsect_chainage:
        try:
            while chainage > n[lchainage_col]:
                p = n
                n = next(rows)
                r = 1 if p is None else (chainage - p[lchainage_col]) / (n[lchainage_col] - p[lchainage_col])
        except StopIteration:
            r = 0
        xsects_result.append((
            chainage,
            p[lbase_cols[0]] + (n[lbase_cols[0]] - p[lbase_cols[0]]) * r,
            p[lbase_cols[1]] + (n[lbase_cols[1]] - p[lbase_cols[1]]) * r,
            p[lend_cols[0]] + (n[lend_cols[0]] - p[lend_cols[0]]) * r,
            p[lend_cols[1]] + (n[lend_cols[1]] - p[lend_cols[1]]) * r
        ))

    return pd.DataFrame(xsects_result, columns=[chainage_col, base_cols[0], base_cols[1], end_cols[0], end_cols[1]])


def points_on_xsects(df_xsects, base_cols, end_cols, point_cols, xs):
    """
    Given a DataFrame of cross-sections and a series of 'x' values along the cross-section, return a series of points
    :param df_xsects: a DataFrame with cross-sections, containing at least base_cols, end_cols
    :param base_cols: x, y tuple of column names indicating the start of a cross-section
    :param end_cols: x, y tuple of column names indicating the end of a cross-section
    :param point_cols: the names for the x, y columns of the return Dataframe
    :param xs: the 'x' distance along the cross-section, x can exceed the length of the cross-section or be negative
    :return: a Dataframe with x, y columns point_cols of points
    """
    lengths = (
        (df_xsects[end_cols[0]] - df_xsects[base_cols[0]]) ** 2 +
        (df_xsects[end_cols[1]] - df_xsects[base_cols[1]]) ** 2
    ).apply(sqrt)

    lbase_cols = (df_xsects.columns.get_loc(base_cols[0]), df_xsects.columns.get_loc(base_cols[1]))
    lend_cols = (df_xsects.columns.get_loc(end_cols[0]), df_xsects.columns.get_loc(end_cols[1]))

    points_result = []
    for xsect, x, length in zip(df_xsects.itertuples(index=False), xs, lengths):
        r = x / length
        points_result.append((
            xsect[lbase_cols[0]] + (xsect[lend_cols[0]] - xsect[lbase_cols[0]]) * r,
            xsect[lbase_cols[1]] + (xsect[lend_cols[1]] - xsect[lbase_cols[1]]) * r
        ))

    return pd.DataFrame(points_result, columns=point_cols)


def main():
    # load GRID and write as wkt geo csv
    df_grid = pd.read_csv('../FEWS_copy/GC_erosion/Modules/evo/gc_expected/geo/GC_EVO_GRID.csv')
    pd.concat([df_grid, wkt_line(df_grid, [('BASE_X', 'BASE_Y'), ('END_X', 'END_Y')])], axis=1).to_csv(
        '../output/GC_EVO_GRID_wkt.csv', index=False)

    # find cross-sections on GRID, matching the seawall chainage, write as wkt geo csv
    df_xsects_chainage = pd.read_csv('../FEWS_copy/GC_erosion/Modules/evo/gc_expected/geo/xsect_ids.csv')
    df_xsects = xsects(
        df_grid, 'CHAINAGE', ('BASE_X', 'BASE_Y'), ('END_X', 'END_Y'), df_xsects_chainage['CHAINAGE'])
    pd.concat([df_xsects_chainage, wkt_line(df_xsects, [('BASE_X', 'BASE_Y'), ('END_X', 'END_Y')])],
              axis=1).to_csv('../output/GC_EVO_GRID_xsects_wkt.csv', index=False)

    # reusing seawall cross-sections, determine seawall points on those cross-sections
    df_seawall_chainage = pd.read_csv('../FEWS_copy/GC_erosion/Modules/evo/gc_expected/geo/GC_EVO_SEAWALL.csv')
    df_seawall_xsects = xsects(
        df_grid, 'CHAINAGE', ('BASE_X', 'BASE_Y'), ('END_X', 'END_Y'), df_seawall_chainage['CHAINAGE'])
    df_seawall_points = points_on_xsects(df_seawall_xsects, ('BASE_X', 'BASE_Y'), ('END_X', 'END_Y'), ('X', 'Y'),
                                         df_seawall_chainage['WALL_X'])
    df_seawall_points['WKT'] = wkt_point(df_seawall_points, ('X', 'Y'))
    pd.concat([df_seawall_points, df_seawall_chainage], axis=1).to_csv('../output/GC_EVO_SEAWALL_wkt.csv', index=False)


if __name__ == '__main__':
    main()
