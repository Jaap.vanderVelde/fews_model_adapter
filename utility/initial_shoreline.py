import logging
from sys import argv
from pathlib import Path
from process_evo import EvoProcessing


def exit_with_error(msg, code=1):
    logging.error(msg)
    exit(code)


def main():
    if len(argv) < 2:
        exit_with_error('initial_shoreline.py requires one argument: the location of the FEWS runinfo.xml')
    if not Path(argv[1]).is_file():
        exit_with_error(f'No runinfo.xml found at: {argv[1]}')

    try:
        ep = EvoProcessing(argv[1])

        if argv[1].lower() == 'pre':
            ep.pre_process()
        elif argv[1].lower() == 'post':
            ep.post_process()
        elif argv[1].lower() == 'shoreline':
            ep.initial_shoreline()
        else:
            ep.post_process2()
    except Exception as e:
        exit_with_error(f'Unexpected exception raised: {e}')

    logging.info('EVO processing complete')


if __name__ == '__main__':
    main()
