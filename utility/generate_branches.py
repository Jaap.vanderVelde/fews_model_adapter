from sys import argv
from csv import reader
from lxml import etree

CSV_NAME = 'input/ALL_PROFILES.csv' if len(argv) < 2 else argv[1]
XML_NAME = '../output/Branches.xml' if len(argv) < 3 else argv[2]

XML_TEMPLATE = 'empty_branches.xml'

CHAINAGE = '<pt chainage="{chainage}" label="{label}" x="{x]" y="{y}"/>\n'


def main():
    with open(CSV_NAME, 'r') as f:
        data = reader(f)
        headers = {header.strip(): index for index, header in enumerate(next(data))}

        _PI_ns = {'FEWS': 'http://www.wldelft.nl/fews'}

        parser = etree.XMLParser(remove_blank_text=True)
        doc = etree.parse(XML_TEMPLATE, parser)
        branches = doc.getroot()
        branch = None

        branch_id = ''
        for rec in data:
            if branch_id != rec[headers['branchid']]:
                if branch is not None:
                    start_chainage.text = str(start)
                    end_chainage.text = str(end)
                    branches.append(branch)
                # new branch
                branch_id = rec[headers['branchid']]
                branch = etree.Element('branch', nsmap=_PI_ns)
                branch.attrib['id'] = branch_id

                branch_name = etree.Element('branchName')
                branch_name.text = branch_id
                branch.append(branch_name)

                start = float(rec[headers['chainage']])
                end = float(rec[headers['chainage']])

                start_chainage = etree.Element('startChainage')
                branch.append(start_chainage)

                end_chainage = etree.Element('endChainage')
                branch.append(end_chainage)

            # keep track of min/max chainage for current branch
            chainage = float(rec[headers['chainage']])
            start = min(start, chainage)
            end = max(end, chainage)

            # add a pt to the branch
            pt = etree.Element('pt', nsmap=_PI_ns)
            pt.attrib['chainage'] = f'{chainage:.1f}'
            pt.attrib['label'] = rec[headers['reach_segment_id']]
            pt.attrib['x'] = rec[headers['x']]
            pt.attrib['y'] = rec[headers['y']]
            pt.attrib['z'] = rec[headers['z']]
            if rec[headers['A-line']] == '1':
                pt.attrib['description'] = 'A-line'
            branch.append(pt)

        if branch is not None:
            start_chainage.text = f'{start:.1f}'
            end_chainage.text = f'{end:.1f}'
            branches.append(branch)

    with open(XML_NAME, 'wb') as f:
        doc.write(f, pretty_print=True)


if __name__ == "__main__":
    main()
