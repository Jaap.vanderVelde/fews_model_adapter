from csv import reader

with open(r'F:\BMT\cogc\FEWS\GC_erosion\Modules\evo\gc_expected\geo\GC_EVO_SEAWALL.csv') as fw:
    with open(r'F:\BMT\cogc\FEWS\GC_erosion\Modules\evo\gc_expected\geo\EVO_PostProcess_Profile_Offset.csv') as fo:
        with open(r'../output/EVO_PostProcess_Profile_Offset.csv', 'w') as fres:
            crw = reader(fw)
            cro = reader(fo)
            next(crw)
            next(cro)
            fres.write(f'EVO_CHAINAGE,A-Line Offset\n')
            for w, o in zip(crw, cro):
                assert float(w[0]) == float(o[0]), f'mismatch between files {w[0]}, {o[0]}'
                fres.write(f'{o[0]},{round(float(o[1])-float(w[1]), 2)}\n')
