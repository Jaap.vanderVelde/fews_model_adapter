# Delft-FEWS Model Adapter

Currently specific to EVO, the script is a model adapter for Delft-FEWS, performing the pre- and post-processing for an EVO model run.

Also included is a utility script to generate the Delft-FEWS `branches.xml` from the a EVO `all_profiles.csv` and another utility script `evo_geo_files.py` script that takes EVO `geo` `.csv` inputs and transforms them into `.csv` files with well-known text (`wkt`) elements for use in GIS.  

## Setup

To be able to use the adapter, the following is required:
- a correctly configured Delft-FEWS with a module set up for EVO, including a model folder with the typical folder structure for an EVO model;
- a Python virtual environment with the requirements ([requirements.txt](requirements.txt)) for this Python project installed, which needs to be active when running the script;
- the script expects to be passed the location of a valid `runinfo.xml`, that at least has the content as documented in [runinfo.xml](FEWS_copy/GC_erosion/Modules/evo/gc_test/to_evo/runinfo.xml);
- the working directory of the model (typically where the `.evc` is expected) needs to contain a copy of `_template.evc` and a copy of `projection.wkt`; this working directory is specified in the `runinfo.xml` as `workDir`;
- `_template.evc` should be updated to match the actual `.evc` for the model, but using all the appropriate substitutions as provided in the example.

To get the script create a virtual environment and install the requirements, use something like:
```cmd
cd Modules
git clone https://gitlab.com/Jaap.vanderVelde/fews_model_adapter.git evo_model_adapter
python -m venv evo_model_adapter_venv
call evo_model_adapter_venv\Scripts\activate.bat
pip install -r evo_model_adapter\requirements.txt
```
Once obtained, ensure a copy of the `_template.evc`, `projection.wkt` and `xsect_ids.csv` are copied to the corresponding locations in the model folder and the configuration is updated accordingly to refer to them correctly in the `runinfo.xml`.

An example `run_pre.bat` matching the example above, could kept in `evo_model_adapter`:
```cmd
@echo off
setlocal

call ..\evo_model_adapter_venv\Scripts\activate.bat
cd ..\evo\model_name\work
python ..\..\..\evo_model_adapter\process_evo.py pre ..\to_evo\runinfo.xml
```
## Run

```cmd
python process_evo.py pre FEWS_region\Modules\evo\gc\to_evo\runinfo.xml
python process_evo.py post2 FEWS_region\Modules\evo\gc\to_evo\runinfo.xml
```

If a log location is configured in the runinfo, that's where the script log will be written. Otherwise, the script assumes a log folder exists in the working directory of the model (the default log location for an EVO model).

## Utilities

Extra scripts have been provided for some additional tasks:

[compute_offset.py](utility/compute_offset.py) was used to compute the offset between the seawall and an initial 'process A-line' file, which was replaced with a profile offset file in a later stage. This script should no longer be needed, unless a new profile offset needs to be computed from a deprecated A-line file.

[evo_geo_files.py](utility/evo_geo_files.py) takes an EVO model and generates shape files for the grid lines, cross-shore sections and the location of the seawall (A-line) on those sections. This is helpful when debugging shoreline outputs.

[generate_branches.py](utility/generate_branches.py) computes a new branches.xml for the Delft-FEWS configuration, based on survey data.

[initial_shoreline.py](utility/initial_shoreline.py) (calls `process_evo`, you can also run `process_evo shoreline runinfo.xml` for the same result) Takes EVO model output (from the runinfo.xml) and generates an initial shoreline based on the very first timestep in the EVO outputs.

Note: if future changes cause the model to be reinitialised off of a new initial warm state, if the seawall location is updated in `GC_EVO_SEAWALL.csv` or if the profile offsets are adjusted in `EVO_PostProcess_Profile_Offset.csv`, this script should be re-run. Note that this means the initial warm state needs to remain available, as the model would have to be re-run for its very first time step after the warm state.
